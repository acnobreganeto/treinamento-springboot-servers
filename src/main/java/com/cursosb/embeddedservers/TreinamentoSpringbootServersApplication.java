package com.cursosb.embeddedservers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TreinamentoSpringbootServersApplication {
	public static void main(String[] args) {
		SpringApplication.run(TreinamentoSpringbootServersApplication.class, args);
	}

	@GetMapping
	String status() {
		return "I'm UP!";
	}
}
